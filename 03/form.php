<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>
	<?php
		$error = [];
		$u = 'admin';
		$p = '123456';

		if (isset($_POST['submit'])) {
			$username = isset($_POST['username']) ? $_POST['username'] : '';
			$password = isset($_POST['password']) ? $_POST['password'] : '';

			//if ($username == '') {
			if (empty($username)) {
				$error[] = 'Vui long nhap Tai khoan';
			}

			if (empty($password)) {
				$error[] = 'Vui long nhap Mat khau';
			}

			if (count($error) == 0) {
				if ($username == $u && $password == $p) {
					echo "Dang nhap thanh cong";
				} else {
					$error[] = 'Sai thong tin dang nhap';
				}
			}
		}	
	?>

	<?php if (count($error) > 0) { ?>
	<div class="message">
		<?php for ($i = 0; $i < count($error); $i++) : ?>		
		<p style="color:red"><?php echo $error[$i];?></p>
		<?php endfor; ?>
	</div>
	<?php } ?>


	<?php 
		/*if (count($error) > 0) {
			$msg = '<div class="message">';
			for ($i = 0; $i < count($error); $i++) {
				$msg .= '<p style="color:red">' .$error[$i] . '</p>';	
			}
			$msg .= '</div>';
			echo $msg;
		}*/
	?>

	<form method="POST" action="">
		<input type="text" name="username" placeholder="Nhap tai khoan" <?php if (isset($_POST['username'])) echo $_POST['username'];?> value="<?php if (isset($_POST['username'])) echo $_POST['username'];?>">
		<input type="password" name="password" placeholder="Nhap mat khau" value="<?php if (isset($_POST['password'])) echo $_POST['password'];?>">
		<button type="submit" name="submit">Dang nhap</button>
	</form>
</body>
</html>


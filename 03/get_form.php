<!DOCTYPE html>
<html>
<head>
	<title>Form GET</title>
</head>
<body>
	<?php
		$error = [];
		$u = 'admin';
		$p = '123456';

		if (isset($_GET['submit'])) {
			$username = isset($_GET['username']) ? $_GET['username'] : '';
			$password = isset($_GET['password']) ? $_GET['password'] : '';

			//if ($username == '') {
			if (empty($username)) {
				$error[] = 'Vui long nhap Tai khoan';
			}

			if (empty($password)) {
				$error[] = 'Vui long nhap Mat khau';
			}

			if (count($error) == 0) {
				if ($username == $u && $password == $p) {
					echo "Dang nhap thanh cong";
				} else {
					$error[] = 'Sai thong tin dang nhap';
				}
			}
		}	
	?>

	<?php if (count($error) > 0) { ?>
	<div class="message">
		<?php for ($i = 0; $i < count($error); $i++) : ?>		
		<p style="color:red"><?php echo $error[$i];?></p>
		<?php endfor; ?>
	</div>
	<?php } ?>

	<form method="GET" action="">
		<input type="text" name="username" placeholder="Nhap tai khoan" value="<?php if (isset($_POST['username'])) echo $_POST['username'];?>">
		<input type="password" name="password" placeholder="Nhap mat khau" value="<?php if (isset($_POST['password'])) echo $_POST['password'];?>">
		<button type="submit" name="submit">Dang nhap</button>
	</form>
</body>
</html>


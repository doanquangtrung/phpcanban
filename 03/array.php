<?php 
	
	//Khai bao mang. 
	//C1:
	$num = [];
	$num[] = 3;
	$num[] = 10;
	$num[] = 20;

	print_r($num);
	//C2:
	$num = [3,10, 20];
	print_r($num);

	//C3
	$num = array();
	$num[] = 5;
	$num[] = 50;
	$num[] = 55;
	$num[] = 99;

	if (in_array(99, $num)) {
		echo 'So 99 co trong mang';
	} else {
		echo "So 99 khong ton tai trong mang";
	}

	// Duyet mang
	for ($i = 0; $i < count($num); $i++) {
		echo $num[$i] . '<br>';
	}

	$sinhvien = [
		'id' => 2,
		'name' => 'Tuan',
		'age' => 20
	];
	print_r($sinhvien);

	$questions = [
		[
			'id' => 1,
			'title' => '1+1 = ?',
			'options' => ['1', '2', '3', '4']
		],
		[
			'id' => 2,
			'title' => '4+1 = ?',
			'options' => ['1', '6', '5', '4']
		]
	];

	print_r($questions);
	echo "<br>";
	echo $questions[1]['title'];
?>